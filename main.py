#download dataset 

import nltk
from nltk.corpus import brown

nltk.download("brown")
nltk.download("universal_tagset")

corpus = brown.tagged_sents(tagset="universal")



# split data into texts and tags

text = []
tag = []

for sequence in corpus:
  text2 = []
  tag2 = []
  for pair in sequence:
    text2.append(pair[0])
    tag2.append(pair[1])

  text.append(text2)
  tag.append(tag2)



#handle data

# turn text into np array
import numpy as np
import pandas as pd
text = np.array(text)

tag = np.array(tag)


# tokenize tag and text

from tensorflow.keras.preprocessing.text import Tokenizer

text_tokenizer = Tokenizer(oov_token=0)

text_tokenizer.fit_on_texts(text)

text = text_tokenizer.texts_to_sequences(text)

tag_tokenizer = Tokenizer(oov_token=0)

tag_tokenizer.fit_on_texts(tag)

tag = tag_tokenizer.texts_to_sequences(tag)


# pad sentence

total_words = len(text_tokenizer.word_index) + 1
total_tags = len(tag_tokenizer.word_index) +1

from tensorflow.keras.preprocessing.sequence import pad_sequences

long = max(text, key=len)

text = pad_sequences(text, padding = 'pre' , maxlen = len(long)+1)

maximum = max(tag, key=len)

tag = pad_sequences(tag, padding = 'pre' , maxlen = len(long)+1)



# test train split

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(text, tag, test_size=0.33, random_state=42)



# train and fit model

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Embedding, LSTM, Dense, Bidirectional, Dropout, GlobalMaxPooling1D, SimpleRNN
from tensorflow.keras.layers import Convolution1D as Conv1D
from tensorflow import keras


model = Sequential()
model.add(Embedding(total_words, 50, input_length = len(long)+1, mask_zero = True))
model.add(Bidirectional(LSTM(32, return_sequences = True)))
model.add(Dense(14, activation = "softmax"))


model.compile(
optimizer= keras.optimizers.Adam(), 
loss= keras.losses.SparseCategoricalCrossentropy(from_logits= True), 
metrics= ['accuracy'])

history = model.fit(X_train,y_train, epochs = 2)



# prediction


seed = "good luck finding people you need for 10k"


seed = seed.split()


seed2 = text_tokenizer.texts_to_sequences(seed)


seed_list = []

for x in seed2:
  for y in x:
    seed_list.append(y)


seed_list = pad_sequences([seed_list], padding = 'pre' , maxlen = len(long)+1)

answer = model.predict(seed_list)

prediction = answer[0][-8:]

    

answer_list = []
for x in prediction:
  maximum = np.argmax(x)
  answer_list.append(maximum)
  

print(answer_list)




